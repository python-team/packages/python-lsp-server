python-lsp-server (1.12.0-3) unstable; urgency=medium

  * Increase test timeout

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 25 Nov 2024 11:53:03 +0100

python-lsp-server (1.12.0-2) unstable; urgency=medium

  * Retry for dgit

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 24 Nov 2024 11:10:28 +0100

python-lsp-server (1.12.0-1) unstable; urgency=medium

  * New upstream version 1.12.0
  * Rediff patches
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 18 Nov 2024 08:12:38 +0100

python-lsp-server (1.10.1-1) unstable; urgency=medium

  * New upstream version 1.10.1
  * Enable jedi tests again

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 25 Mar 2024 11:06:41 +0100

python-lsp-server (1.10.0-1) unstable; urgency=medium

  * New upstream version 1.10.0
  * Rediff patches
  * Ignore jedi being broken (already broken in the archive)

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 07 Feb 2024 13:20:31 +0100

python-lsp-server (1.9.0-1) unstable; urgency=medium

  * New upstream version 1.9.0
  * Rebase patches
  * Disable broken tests

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 15 Nov 2023 10:03:29 +0100

python-lsp-server (1.7.4-1) unstable; urgency=medium

  * New upstream version 1.7.4
  * Rediff patches
  * Fix python3-pydocstyle version (Closes: #1039616)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 01 Jul 2023 14:55:12 +0200

python-lsp-server (1.7.3-1) unstable; urgency=medium

  * New upstream version 1.7.3
  * Rediff patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 13 Jun 2023 18:37:34 +0200

python-lsp-server (1.7.1-1) unstable; urgency=medium

  * New upstream version

 -- Julian Gilbey <jdg@debian.org>  Wed, 18 Jan 2023 09:26:58 +0000

python-lsp-server (1.7.0-2) unstable; urgency=medium

  * Fix missing pylint category (reported and accepted upstream:
    https://github.com/python-lsp/python-lsp-server/pull/334)

 -- Julian Gilbey <jdg@debian.org>  Sun, 08 Jan 2023 18:28:13 +0000

python-lsp-server (1.7.0-1) unstable; urgency=medium

  * New upstream version

 -- Julian Gilbey <jdg@debian.org>  Thu, 05 Jan 2023 19:14:25 +0000

python-lsp-server (1.6.0-2) unstable; urgency=medium

  * Enable autopkgtest-pkg-pybuild

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 10 Dec 2022 09:38:37 +0100

python-lsp-server (1.6.0-1) unstable; urgency=medium

  * New upstream version 1.6.0
  * Drop patch (merged upstream)
  * Add new build dependency

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 03 Nov 2022 11:40:35 +0100

python-lsp-server (1.5.0-3) unstable; urgency=medium

  * Drop patch for old setuptools
  * Add upstream patch for new pylint (Closes: #1022391)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 23 Oct 2022 16:28:20 +0200

python-lsp-server (1.5.0-2) unstable; urgency=medium

  * Add Breaks: spyder clause

 -- Julian Gilbey <jdg@debian.org>  Fri, 22 Jul 2022 17:19:49 +0100

python-lsp-server (1.5.0-1) unstable; urgency=medium

  * New upstream version 1.5.0
  * Update dependencies
  * Bump policy version (no changes)
  * Add patch to workaound old setuptools
  * Fix tests

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 11 Jul 2022 16:39:13 +0200

python-lsp-server (1.4.1-2) unstable; urgency=medium

  * Add versioned dependency for pluggy (Closes: #1009795)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 18 Apr 2022 11:00:15 +0200

python-lsp-server (1.4.1-1) unstable; urgency=medium

  * New upstream version 1.4.1

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 29 Mar 2022 09:56:40 +0200

python-lsp-server (1.4.0-1) unstable; urgency=medium

  * New upstream version 1.4.0
  * Drop patches (applied upstream)
  * Add new build dependencies

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 15 Mar 2022 22:13:58 +0100

python-lsp-server (1.3.3-2) unstable; urgency=medium

  * Add patch to fix build

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 16 Dec 2021 17:18:20 +0100

python-lsp-server (1.3.3-1) unstable; urgency=medium

  * New upstream version 1.3.3

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 16 Dec 2021 11:29:53 +0100

python-lsp-server (1.3.2-1) unstable; urgency=medium

  * New upstream version 1.3.2
  * Drop patch (applied upstream)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 26 Nov 2021 07:57:13 +0100

python-lsp-server (1.2.4-4) unstable; urgency=medium

  * Add a second Python 3.10 patch

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 05 Nov 2021 22:47:01 +0100

python-lsp-server (1.2.4-3) unstable; urgency=medium

  * Add patch for Python 3.10

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 05 Nov 2021 12:28:29 +0100

python-lsp-server (1.2.4-2) unstable; urgency=medium

  * Hide CI variable in d/rules
  * Add upstream patch for pylint>=2.10 (Closes: #998526)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 05 Nov 2021 07:39:40 +0100

python-lsp-server (1.2.4-1) unstable; urgency=medium

  * New upstream version 1.2.4

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 13 Oct 2021 11:36:25 +0200

python-lsp-server (1.2.3-1) unstable; urgency=medium

  * New upstream version 1.2.3
  * Drop patch (applied upstream)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 05 Oct 2021 08:34:38 +0200

python-lsp-server (1.2.2-2) unstable; urgency=medium

  * source only upload

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 19 Sep 2021 15:03:13 +0200

python-lsp-server (1.2.2-1) unstable; urgency=medium

  [ Julian Gilbey ]
  * Remove Otto from uploaders

  [ Pablo Mestre Drake ]
  * Update debian/control     - (Standards-Version): Migrate to 4.6.0

  [ Jochen Sprickerhof ]
  * Switch to python-lsp fork (Closes: #994007)
  * New upstream version 1.2.2
  * Update copyright
  * Rework patches
  * Rename binary package

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 17 Sep 2021 22:40:52 +0200

python-language-server (0.36.2-3) unstable; urgency=medium

  * Add pylint to Suggests field

 -- Julian Gilbey <jdg@debian.org>  Fri, 05 Feb 2021 08:57:24 +0000

python-language-server (0.36.2-2) unstable; urgency=medium

  * Use pypi for upstream package
  * Add patch for version information
  * Use dh-sequence-python3

 -- Julian Gilbey <jdg@debian.org>  Thu, 04 Feb 2021 06:20:02 +0000

python-language-server (0.36.2-1) unstable; urgency=medium

  * Initial release (Closes: #963605)
  * Apply upstream PR #901 to improve jedi compatibility

 -- Julian Gilbey <jdg@debian.org>  Sun, 24 Jan 2021 07:02:17 +0000
